package com.tokenlab.luizmoura.myfavoritegames;

import android.os.Bundle;
import android.widget.TextView;

import com.tokenlab.luizmoura.myfavoritegames.util.BaseActivity;

/**
 * Created by Yuno on 03/10/2017
 * at project MyFavoriteGames.
 */

public class AboutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        ((TextView) findViewById(R.id.versionView)).setText(getString(R.string.version, BuildConfig.VERSION_NAME));
    }

}
