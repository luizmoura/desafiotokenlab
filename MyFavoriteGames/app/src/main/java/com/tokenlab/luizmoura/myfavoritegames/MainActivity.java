package com.tokenlab.luizmoura.myfavoritegames;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.tokenlab.luizmoura.myfavoritegames.adapter.GameListAdapter;
import com.tokenlab.luizmoura.myfavoritegames.data.Game;
import com.tokenlab.luizmoura.myfavoritegames.internet.GetJsonTask;
import com.tokenlab.luizmoura.myfavoritegames.internet.JsonContract;
import com.tokenlab.luizmoura.myfavoritegames.util.BaseActivity;
import com.tokenlab.luizmoura.myfavoritegames.util.C;
import com.tokenlab.luizmoura.myfavoritegames.util.L;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends BaseActivity {

    private GameListAdapter adapter;

    private View loadingView;

    private class OnlineCallback implements GetJsonTask.phpInterface {
        @Override
        public void result(JSONObject jsonObject) throws JSONException {
            JSONArray games = jsonObject.getJSONArray(JsonContract.GAMES.INDEX);

            Long openid = -1L;
            if (getIntent().getAction() != null) {
                // in case app was opened via link, check if is a "valid link" and tries to get the id
                if (getIntent().getAction().equals(Intent.ACTION_VIEW) &&
                        getIntent().getData().toString().contains("open.luizmouragames.com")) {
                    try {
                        openid = Long.valueOf(getIntent().getData().getLastPathSegment());
                    } catch (Exception e) {
                        openid = -1L;
                    }
                }
            }

            for (int i = 0; i < games.length(); i++) {
                try {
                    Game game = new Game(games.getJSONObject(i));
                    adapter.add(game);

                    if(game.getId().equals(openid)) {
                        // if id matchs, open new Activity. Only works if app is opened via link
                        Intent intent = new Intent(getBaseContext(), GameDetailActivity.class);
                        intent.putExtra(GameDetailActivity.EXTRA_GAME, adapter.getItem(i));
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    //Should never trigger
                    L.e(TAG, "JSON", e);
                }
            }

            loadingView.setVisibility(View.GONE);
        }

        @Override
        public void error(int error) {
            loadingView.setVisibility(View.GONE);
        }
    }

    private class Listener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent intent = new Intent(getBaseContext(), GameDetailActivity.class);
            intent.putExtra(GameDetailActivity.EXTRA_GAME, adapter.getItem(i));
            startActivity(intent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(C.loggedUser == null) {
            // Didn't pass through the Splash (was opened via link)
            Intent i = new Intent(this, SplashActivity.class);
            i.putExtra(SplashActivity.EXTRA_NEED_MAIN, false);
            startActivity(i);
        }

        C.unavaliableInfo = getString(R.string.unavaliable_info);

        loadingView = findViewById(R.id.loadingView);
        loadingView.setVisibility(View.VISIBLE);

        adapter = new GameListAdapter(getBaseContext());

        ListView listView = (ListView) findViewById(R.id.gameListView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new Listener());
        listView.setEmptyView(findViewById(R.id.listErrorView));

        GetJsonTask task = new GetJsonTask(new OnlineCallback());
        task.execute(C.API_GAME);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_profile: {
                Intent intent = new Intent(getBaseContext(), ProfileActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.action_about: {
                Intent intent = new Intent(getBaseContext(), AboutActivity.class);
                startActivity(intent);
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }
}
