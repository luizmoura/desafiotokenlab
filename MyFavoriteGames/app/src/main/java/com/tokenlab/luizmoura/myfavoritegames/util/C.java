package com.tokenlab.luizmoura.myfavoritegames.util;

import com.tokenlab.luizmoura.myfavoritegames.data.User;

import java.util.Date;

/**
 * Constants Class
 *
 * Created by Yuno on 03/10/2017
 * at project MyFavoriteGames.
 */

public class C {

    static final String LOGTAG = "YunoLog";

    public static final String API_GAME = "https://dl.dropboxusercontent.com/s/1b7jlwii7jfvuh0/games";
    public static final String API_USER = "https://dl.dropboxusercontent.com/s/fiqendqz4l1xk61/userinfo";

    // This is needed because it is used in places where Context could be null (in object constructors)
    // It is definied in MainsActivity
    public static String unavaliableInfo = "-";
    public static Date nullDate = new Date(0);

    public static User loggedUser;
    public static final int PREF_VALID = 10*60*1000; // 10 minutes

    public static final String SHARED_PREF_USER = "moura.myfavoritegames.user_preferences";
    /** Json stringfied that holds user information */
    public static final String PREF_USER_INFO = "moura.myfavoritegames.userinfo_preference";
    /** Date when the PREF_USER_INFO was saved (used to invalidate the pref) */
    public static final String PREF_USER_DATE = "moura.myfavoritegames.userdate_preference";

}
