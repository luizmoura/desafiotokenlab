package com.tokenlab.luizmoura.myfavoritegames.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tokenlab.luizmoura.myfavoritegames.R;
import com.tokenlab.luizmoura.myfavoritegames.data.Game;

/**
 * Created by Yuno on 03/10/2017
 * at project MyFavoriteGames.
 */

public class GameListAdapter extends ArrayAdapter<Game>{

    private Context context;

    private class Holder {
        TextView title;
        TextView release;
        ImageView image;

        Holder(TextView title, TextView release, ImageView image) {
            this.image = image;
            this.title = title;
            this.release = release;
        }
    }

    public GameListAdapter(@NonNull Context context) {
        super(context, 0);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        Holder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.game_list_item, parent, false);
            holder = new Holder(
                (TextView) convertView.findViewById(R.id.titleView),
                (TextView) convertView.findViewById(R.id.dateView),
                (ImageView) convertView.findViewById(R.id.iconView)
            );

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        Game game = getItem(position);
        if(game != null) {
            holder.title.setText(game.getName());
            holder.release.setText(game.getFormattedReleaseDate(false));
            game.getImageLoader(context).into(holder.image);
        }

        return convertView;
    }

}
