package com.tokenlab.luizmoura.myfavoritegames.internet;

import android.os.AsyncTask;

import com.tokenlab.luizmoura.myfavoritegames.BuildConfig;
import com.tokenlab.luizmoura.myfavoritegames.R;
import com.tokenlab.luizmoura.myfavoritegames.util.L;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

/**
 * This class handles http connections
 * Created by Luiz on 09/11/2015.
 */
public class GetJsonTask extends AsyncTask<String, Void, JSONObject> {

    private static final String TAG = "YunoTask";
    private static final int MIN_TIME_NORMAL = 5*1000;

    private static HashMap<String, Historico> mapHistory;

    private HttpURLConnection conn;

    private static class Historico {
        final Calendar time_request;
        final String result;

        Historico(Calendar time_request, String result) {
            this.time_request = time_request;
            this.result = result;
        }

        Calendar getTime_request() { return time_request; }
        String getResult() { return result; }
    }

    private final phpInterface callback;
    public interface phpInterface {
        /** sucesso */
        void result(JSONObject jsonObject) throws JSONException;
        void error(int error);
    }

    public GetJsonTask(phpInterface activity){
        if(mapHistory == null) {
            mapHistory = new HashMap<>();
        }
        callback = activity;
    }

    @Override
    protected JSONObject doInBackground(String... params) {

        String result;
        int error = 0;

        try {
            String sParam = Arrays.toString(params);

            if (BuildConfig.DEBUG) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    L.e(TAG, "Não foi possivel chamar sleep()");
                }
            }

            // impede muitas chamadas seguidas ao servidor
            if(mapHistory.keySet().contains(sParam)) {
                long time_since_last = Calendar.getInstance().getTimeInMillis() - mapHistory.get(sParam).getTime_request().getTimeInMillis();
                // usa o ultimo resultado caso a ultima chamada ocorreu recentemente
                if(time_since_last < MIN_TIME_NORMAL) {
                    L.d(TAG, "Using memory " + time_since_last + " / " + MIN_TIME_NORMAL);
                    return new JSONObject(mapHistory.get(sParam).getResult());
                }
            }

            L.d(TAG, "Requesting: " + Arrays.toString(params));
            URL url = new URL(params[0]);
            conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("POST");
            conn.setRequestProperty("User-Agent", "*/*");
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);

            // enviando parametros
            // '&' deve estar entre aspas simples
            conn.setDoOutput(true);
            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
            for (int i = 1; i < params.length; i++)
                osw.write('&' + params[i]);
            osw.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                L.e(TAG, conn.getResponseCode() + " : " + conn.getResponseMessage());
                return null;
            }

            if(!isCancelled()) {
                // recebendo resposta
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                StringBuilder response = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null)
                    response.append(line).append("\n");

                // parseia o resultado no formato JsonArray
                result = response.toString();

                // salva o resultado para possivel uso futuro
                mapHistory.put(sParam, new Historico(Calendar.getInstance(), result));

                return new JSONObject(result);
            }

        } catch (MalformedURLException e) {
            L.wtf(TAG, "URL problems", e);
            if (!isCancelled())
                error = R.string.e_internet_php_url;
        } catch (SocketTimeoutException e) {
            L.e(TAG, "Timeout", e);
            if (!isCancelled())
                error = R.string.e_internet_php_timeout;
        } catch (IOException e) {
            L.e(TAG, "IO error", e);
            if (!isCancelled())
                error = R.string.e_internet_php_io;
        } catch (JSONException e) {
            L.e(TAG, "Response couldn't be converted to JSON", e);
            if (!isCancelled())
                error = R.string.e_internet_php_json;
        }

        if (!isCancelled()) {
            try {
                return new JSONObject("{\"status\":0, \"problem\":"+error+"}");
            } catch (JSONException e) {
                return null;
            }
        }

        return null;

    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        if (conn != null)
            conn.disconnect();

        if (jsonObject == null) {
            return;
        }

        try {
            L.d(TAG, jsonObject.toString(3));
        } catch (JSONException e) {
            L.d(TAG, jsonObject.toString());
        }

        try {
            callback.result(jsonObject);
        } catch (JSONException e) {
            L.e(TAG, "Result error", e);
            callback.error(R.string.e_internet_php_act);
        }

    }
}

