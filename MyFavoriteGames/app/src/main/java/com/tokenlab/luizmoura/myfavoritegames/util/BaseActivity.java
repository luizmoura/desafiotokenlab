package com.tokenlab.luizmoura.myfavoritegames.util;

import android.support.annotation.LayoutRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.tokenlab.luizmoura.myfavoritegames.R;

/**
 * All Activities (except Splash) should extends this class
 * This way, commom functions can be done in here
 * Created by Yuno on 03/10/2017
 * at project MyFavoriteGames.
 */

public class BaseActivity extends AppCompatActivity {
    public static final String TAG = C.LOGTAG + BaseActivity.class.getSimpleName();


    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarView);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(getParentActivityIntent() != null);
            actionBar.setDisplayShowCustomEnabled(true);
            ((TextView) toolbar.findViewById(R.id.titleView)).setText(getTitle());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
