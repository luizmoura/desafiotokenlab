package com.tokenlab.luizmoura.myfavoritegames.data;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.tokenlab.luizmoura.myfavoritegames.R;
import com.tokenlab.luizmoura.myfavoritegames.internet.JsonContract;
import com.tokenlab.luizmoura.myfavoritegames.util.C;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Yuno on 03/10/2017
 * at project MyFavoriteGames.
 */

public class Game implements Parcelable {

    public static final Parcelable.Creator<Game> CREATOR = new Parcelable.Creator<Game>() {
        public Game createFromParcel(Parcel in) {
            return new Game(in);
        }

        public Game[] newArray(int size) {
            return new Game[size];
        }
    };
    private Long id;
    private String name;
    private Date releaseDate;
    private String image_link;
    private String trailer_link;
    private ArrayList<String> plataforms;

    private Game(){
        id = -9L;
        name = C.unavaliableInfo;
        image_link = C.unavaliableInfo;
        trailer_link = C.unavaliableInfo;
        plataforms = new ArrayList<>();
    }

    public Game(JSONObject json) throws JSONException {
        this();
        if (json.has(JsonContract.GAMES.ID)) {
            id = json.getLong(JsonContract.GAMES.ID);
        }
        if (json.has(JsonContract.GAMES.NAME)) {
            name = json.getString(JsonContract.GAMES.NAME);
        }
        try {
            SimpleDateFormat dtFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            releaseDate = dtFormat.parse(json.getString(JsonContract.GAMES.RELEASEDATE));
        } catch (ParseException e) {
            releaseDate = C.nullDate;
        }
        if (json.has(JsonContract.GAMES.IMAGE)) {
            image_link = json.getString(JsonContract.GAMES.IMAGE);
        }
        if (json.has(JsonContract.GAMES.TRAILER)) {
            trailer_link = json.getString(JsonContract.GAMES.TRAILER);
        }
        if (json.has(JsonContract.GAMES.PLATFORMS)) {
            JSONArray array = json.getJSONArray(JsonContract.GAMES.PLATFORMS);
            for (int i = 0; i < array.length(); i++) {
                plataforms.add(array.getString(i));
            }
        }
    }

    private Game(Parcel parcel) {
        this();
        name = parcel.readString();
        releaseDate = new Date(parcel.readLong());
        image_link = parcel.readString();
        trailer_link = parcel.readString();
        plataforms = new ArrayList<>();
        parcel.readStringList(plataforms);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(name);
        parcel.writeLong(releaseDate.getTime());
        parcel.writeString(image_link);
        parcel.writeString(trailer_link);
        parcel.writeStringList(plataforms);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFormattedReleaseDate(boolean formatLong) {
        if (releaseDate == C.nullDate) {
            return C.unavaliableInfo;
        }
        if (formatLong) {
            return SimpleDateFormat.getDateInstance(DateFormat.LONG).format(releaseDate);
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            return dateFormat.format(releaseDate);
        }
    }

    public RequestCreator getImageLoader(Context context) {
        return Picasso.with(context).load(image_link).placeholder(R.drawable.placeholder_load).error(R.drawable.not_found);
    }

    public String getAvailable() {
        String result = "";
        if (plataforms.isEmpty()) {
            return C.unavaliableInfo;
        }

        for (int i = 0; i < plataforms.size(); i++) {
            result += plataforms.get(i) + "\n";
        }
        return result.trim();
    }

    public String getTrailer() {
        return trailer_link;
    }
}
