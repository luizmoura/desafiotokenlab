package com.tokenlab.luizmoura.myfavoritegames;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;

import com.tokenlab.luizmoura.myfavoritegames.data.User;
import com.tokenlab.luizmoura.myfavoritegames.internet.GetJsonTask;
import com.tokenlab.luizmoura.myfavoritegames.util.C;
import com.tokenlab.luizmoura.myfavoritegames.util.L;
import com.tokenlab.luizmoura.myfavoritegames.util.LifecycleHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Yuno on 03/10/2017
 * at project MyFavoriteGames.
 */
public class SplashActivity extends Activity {

    private static final String TAG = "YunoSplash";
    private static int SPLASH_TIME = BuildConfig.DEBUG ? 200 : 1000;

    /** if true or not definied, it will start the MainActivity after finishes itself */
    public static final String EXTRA_NEED_MAIN = "moura.myfavoritegames.needmain_extra";

    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (BuildConfig.DEBUG) {
            LifecycleHandler.instanciate(getApplication());
        }

        final View loading = findViewById(R.id.loadingView);

        sharedPref = getSharedPreferences(C.SHARED_PREF_USER, MODE_PRIVATE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (new Date().getTime() - sharedPref.getLong(C.PREF_USER_DATE, 0) < C.PREF_VALID) {
                    // User data recently downloaded, using data from phone memory
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(sharedPref.getString(C.PREF_USER_INFO, ""));
                        C.loggedUser = new User(jsonObject);
                    } catch (JSONException e) {
                        // The saved data was corrupted. Will call online API to get new version
                        C.loggedUser = null;
                    }
                }

                if(C.loggedUser == null) {
                    GetJsonTask task = new GetJsonTask(new OnlineCallback());
                    task.execute(C.API_USER);
                    loading.setVisibility(View.VISIBLE);
                } else {
                    splashDone();
                }
            }
        }, SPLASH_TIME);

    }

    private void splashDone() {
        if (getIntent().getBooleanExtra(EXTRA_NEED_MAIN, true)) {
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
        }
        finish();
    }

    private class OnlineCallback implements GetJsonTask.phpInterface {

        @Override
        public void result(JSONObject jsonObject) throws JSONException {
            C.loggedUser = new User(jsonObject);

            //Saving info for the next time
            sharedPref.edit()
                    .putString(C.PREF_USER_INFO, jsonObject.toString())
                    .putLong(C.PREF_USER_DATE, new Date().getTime())
                    .apply();

            splashDone();
        }

        @Override
        public void error(int error) {
            L.toast(getBaseContext(), TAG, R.string.e_internet_no_connect);
            finish();
        }
    }
}
