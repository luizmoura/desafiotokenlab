package com.tokenlab.luizmoura.myfavoritegames;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tokenlab.luizmoura.myfavoritegames.data.Game;
import com.tokenlab.luizmoura.myfavoritegames.util.BaseActivity;

/**
 * Created by Yuno on 03/10/2017
 * at project MyFavoriteGames.
 */

public class GameDetailActivity extends BaseActivity {

    /** The Game object this class should display */
    public static final String EXTRA_GAME = "moura.myfavoritegames.game_extra";

    private Game game;

    private class Listener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.linkYouTubeView:
                    youtubeClick();
                    break;
                case R.id.linkWikipediaView:
                    wikipediaClick();
                    break;
            }
        }

        private void youtubeClick(){
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(game.getTrailer()));
            startActivity(intent);
        }

        private void wikipediaClick(){
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(getString(R.string.wikipedia_link, game.getName())));
            startActivity(intent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null) {
            game = savedInstanceState.getParcelable(EXTRA_GAME);
        } else {
            game = getIntent().getParcelableExtra(EXTRA_GAME);
        }

        if (game == null) {
            // Just prevention, in theory will never be required
            finish();
            return;
        }

        setTitle(game.getName());
        setContentView(R.layout.activity_game_detail);
        ((TextView)findViewById(R.id.nameView)).setText(game.getName());
        ((TextView)findViewById(R.id.releaseView)).setText(game.getFormattedReleaseDate(true));
        ((TextView)findViewById(R.id.plataformsView)).setText(game.getAvailable());

        Listener listener = new Listener();
        findViewById(R.id.linkYouTubeView).setOnClickListener(listener);
        findViewById(R.id.linkWikipediaView).setOnClickListener(listener);

        game.getImageLoader(getBaseContext()).into((ImageView) findViewById(R.id.imageView));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(EXTRA_GAME, game);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_share: {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, game.getName(), game.getTrailer()));
                intent.setType("text/plain");
                startActivity(intent);
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

}
