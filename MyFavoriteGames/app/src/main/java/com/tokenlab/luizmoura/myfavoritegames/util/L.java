package com.tokenlab.luizmoura.myfavoritegames.util;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.Toast;

import com.tokenlab.luizmoura.myfavoritegames.BuildConfig;
import com.tokenlab.luizmoura.myfavoritegames.R;

import java.util.Arrays;
import java.util.List;

/**
 * Log and Messaging class
 * Created by Luiz on 27/09/2016.
 */
public class L {
    private static final String TAG = C.LOGTAG + "Log";
    private static Toast toast;

    // region snackbar
    private static void snack(final Activity act, final String message) {
        L.v(TAG, act.getLocalClassName(), " snackbar ", message);

        if (act.findViewById(R.id.container) != null) {
            Snackbar.make(act.findViewById(R.id.container), message, Snackbar.LENGTH_LONG)
                    .show();
        } else {
            L.d(TAG, "Activity doesn't have a view with id/container. Defaulting to DecorView");
            Snackbar.make(act.getWindow().getDecorView(), message, Snackbar.LENGTH_LONG).show();
        }
    }
    // endregion snackbar

    // region toast

    public static void toast(Context context, String tag, String message) {
        // Removes the last toast, if still visible
        // This prevents the app's toast messages floods
        if (toast != null) toast.cancel();

        L.v(TAG, tag, " toast ", message);

        toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.show();
    }

    public static void toast(Context context, String tag, int message) {
        toast(context, tag, context.getString(message));
    }
    // endregion toast

    // region Log
    private static final List<String> skipClass = Arrays.asList(
            "Method.java", "AndroidInstantRuntime.java", // treatment to deal with InstantRun
            L.class.getSimpleName()+".java", // ignoring this custom Log class
            "DatabaseManager.java" // useful to see Select's origin
    );
    /** This will concat <code>(class.java:linenumber)</code> to the message, creating a link to that line in the logcat */
    private static String link(String message){
        if (BuildConfig.DEBUG) {
            int index = 3;
            StackTraceElement stack = Thread.currentThread().getStackTrace()[index];

            while(skipClass.contains(stack.getFileName())) {
                stack = Thread.currentThread().getStackTrace()[++index];
            }
            // Alternative way: pick the 3th method above "link" (funcao > L.d > log > link)
//            stack = Thread.currentThread().getStackTrace()[++index];
//            index = 1;
//            while(!stack.getMethodName().equals("link")) {
//                stack = Thread.currentThread().getStackTrace()[++index];
//            }
//            stack = Thread.currentThread().getStackTrace()[index+3];

            String link = "("+stack.getFileName()+":"+stack.getLineNumber()+")~"+stack.getMethodName();
            message = message.contains("\n") ? link +"\n"+ message : message +" "+ link;
        }
        return message;
    }

    // Object... ensures log removal. More details: http://stackoverflow.com/a/6023505
    /** All Log message should pass through here */
    private static void log(int level, String tag, Object... args) {
        if (BuildConfig.DEBUG) {
            // Verificando se último argumento é um Throwable
            Throwable tr = null;
            if (args[args.length - 1] instanceof Throwable) {
                tr = (Throwable) args[args.length - 1];
                // novo args = antigo args - Throwable
                args = Arrays.copyOf(args, args.length - 1);
            }

            String message = "";
            for(Object s : args) {
                message += s.toString();
            }

            message = link(message);

            switch (level) {
                case Log.VERBOSE:
                    Log.v(tag, message, tr);
                    break;
                case Log.DEBUG:
                    Log.d(tag, message, tr);
                    break;
                case Log.INFO:
                    Log.i(tag, message, tr);
                    break;
                case Log.ERROR:
                    Log.e(tag, message, tr);
                    break;
                case Log.ASSERT:
                    Log.wtf(tag, message, tr);
            }
        }
    }

    public static void v(String tag, Object... message) {
        log(Log.VERBOSE, tag, message);
    }

    public static void d(String tag, Object... message) {
        log(Log.DEBUG, tag, message);
    }

    public static void i(String tag, Object... message) {
        log(Log.INFO, tag, message);
    }

    public static void w(String tag, Object... message) {
        log(Log.WARN, tag, message);
    }

    public static void e(String tag, Object... message) {
        log(Log.ERROR, tag, message);
    }

    public static void wtf(String tag, Object... message) {
        log(Log.ASSERT, tag, message);
    }
    // endregion Log

}