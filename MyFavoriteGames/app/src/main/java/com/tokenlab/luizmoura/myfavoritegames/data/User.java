package com.tokenlab.luizmoura.myfavoritegames.data;

import android.content.Context;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.tokenlab.luizmoura.myfavoritegames.R;
import com.tokenlab.luizmoura.myfavoritegames.internet.JsonContract;
import com.tokenlab.luizmoura.myfavoritegames.util.C;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Yuno on 03/10/2017
 * at project MyFavoriteGames.
 */

public class User {

    private String name = "";
    private String lastname = "";
    private String avatar_link;
    private String email;
    private Date birthday;
    private String address;
    private String city;
    private String country;
    private String token;

    private User (){
        name = "";
        lastname = "";
        email = C.unavaliableInfo;
        birthday = C.nullDate;
        address = C.unavaliableInfo;
        city = C.unavaliableInfo;
        country = C.unavaliableInfo;
        token = C.unavaliableInfo;
    }

    public User(JSONObject json) throws JSONException {
        this();

        if (json.has(JsonContract.USER.NAME)) {
            name = json.getString(JsonContract.USER.NAME);
        }
        if (json.has(JsonContract.USER.LASTNAME)) {
            lastname = json.getString(JsonContract.USER.LASTNAME);
        }
        if (json.has(JsonContract.USER.AVATAR)) {
            avatar_link = json.getString(JsonContract.USER.AVATAR);
        }
        if (json.has(JsonContract.USER.EMAIL)) {
            email = json.getString(JsonContract.USER.EMAIL);
        }
        try {
            birthday = SimpleDateFormat.getDateTimeInstance().parse(JsonContract.USER.BIRTHDAY);
        } catch (ParseException e) {
            birthday = new Date(0);
        }
        if (json.has(JsonContract.USER.ADDRESS)) {
            address = json.getString(JsonContract.USER.ADDRESS);
        }
        if (json.has(JsonContract.USER.CITY)) {
            city = json.getString(JsonContract.USER.CITY);
        }
        if (json.has(JsonContract.USER.COUNTRY)) {
            country = json.getString(JsonContract.USER.COUNTRY);
        }
        if (json.has(JsonContract.USER.TOKEN)) {
            token = json.getString(JsonContract.USER.TOKEN);
        }
    }

    public String getFullName() {
        String fullname = (this.name+" "+this.lastname).trim();
        if (fullname.isEmpty()) {
            return C.unavaliableInfo;
        } else {
            return fullname;
        }
    }

    public String getEmail() {
        return email;
    }

    public String getFormattedBirthday() {
        if (birthday == C.nullDate) {
            return C.unavaliableInfo;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return dateFormat.format(birthday);
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getToken() {
        return token;
    }

    public RequestCreator getAvatarLoader(Context context) {
        return Picasso.with(context).load(avatar_link).placeholder(R.drawable.placeholder_load).error(R.drawable.not_found);
    }

}
