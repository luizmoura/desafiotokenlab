package com.tokenlab.luizmoura.myfavoritegames;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.tokenlab.luizmoura.myfavoritegames.util.BaseActivity;
import com.tokenlab.luizmoura.myfavoritegames.util.C;

/**
 * Created by Yuno on 03/10/2017
 * at project MyFavoriteGames.
 */

public class ProfileActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        C.loggedUser.getAvatarLoader(getBaseContext()).into((ImageView) findViewById(R.id.avatarView));
        ((TextView)findViewById(R.id.nameView)).setText(C.loggedUser.getFullName());
        ((TextView)findViewById(R.id.emailView)).setText(C.loggedUser.getEmail());
        ((TextView)findViewById(R.id.birthdayView)).setText(C.loggedUser.getFormattedBirthday());
        ((TextView)findViewById(R.id.addressView)).setText(C.loggedUser.getAddress());
        ((TextView)findViewById(R.id.cityView)).setText(C.loggedUser.getCity());
        ((TextView)findViewById(R.id.countryView)).setText(C.loggedUser.getCountry());
        ((TextView)findViewById(R.id.tokenView)).setText(C.loggedUser.getToken());

    }

}
