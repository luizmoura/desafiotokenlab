package com.tokenlab.luizmoura.myfavoritegames.util;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

/**
 * Gerencia o Lifecycle das atividades
 * Created by Luiz on 21/07/2015.
 */
public class LifecycleHandler implements Application.ActivityLifecycleCallbacks {

    private final static String TAG = "YunoLifecycle";
    /** Create Start Pause SaveInstance */
    private final boolean[] logNivel = {true, true, true, false};

    /** Singleton.<br/>Use <b>LifecycleHandler.instanciate(getApplication());</b> instead */
    private LifecycleHandler(){}

    private static LifecycleHandler life;
    public static void instanciate(Application app){
        if (life == null) { life = new LifecycleHandler(); }
        else { app.unregisterActivityLifecycleCallbacks(life); }
        app.registerActivityLifecycleCallbacks(life);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        if (logNivel[0]) Log.i(TAG, ">== " + activity.getClass().getSimpleName() + " calling onCreate  ==< ");
    }

    @Override
    public void onActivityStarted(Activity activity) {
//        ++started;
        if (logNivel[1]) Log.i(TAG, "=>= " + activity.getClass().getSimpleName() + " calling onStart   =<=");
    }

    @Override
    public void onActivityResumed(Activity activity) {
//        ++resumed;
        if (logNivel[2]) Log.i(TAG, "==> " + activity.getClass().getSimpleName() + " calling onResume  <==");
    }

    @Override
    public void onActivityPaused(Activity activity) {
//        ++paused;
        if (logNivel[2]) Log.d(TAG, "==< " + activity.getClass().getSimpleName() + " calling onPause   >==");
    }

    @Override
    public void onActivityStopped(Activity activity) {
//        ++stopped;
        if (logNivel[1]) Log.d(TAG, "=<= " + activity.getClass().getSimpleName() + " calling onStop    =>=");
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if (logNivel[0]) Log.d(TAG, "<== " + activity.getClass().getSimpleName() + " calling onDestroy ==>");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        if (logNivel[3]) Log.i(TAG, "=+= " + activity.getClass().getSimpleName() + " calling saveInstance (" + outState.size() + ") =+=");
    }

//    private static int resumed,paused,started,stopped;

//    public static boolean isApplicationVisible(){
//        return started > stopped;
//    }

//    public static boolean isApplicationForeground(){
//        return resumed > paused;
//    }
}
