package com.tokenlab.luizmoura.myfavoritegames.internet;

/**
 * Fields contain in the return from API
 * Created by Yuno on 04/10/2017
 * at project MyFavoriteGames.
 */
public class JsonContract {

    public static class GAMES {
        public static final String INDEX = "games";

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String IMAGE = "image";
        public static final String RELEASEDATE = "release_date";
        public static final String TRAILER = "trailer";
        public static final String PLATFORMS= "platforms";
    }

    public static class USER {
        public static final String NAME = "name";
        public static final String LASTNAME = "lastname";
        public static final String AVATAR = "avatar";
        public static final String EMAIL = "email";
        public static final String BIRTHDAY = "birthday";
        public static final String ADDRESS = "address";
        public static final String CITY = "city";
        public static final String COUNTRY = "country";
        public static final String TOKEN = "token";
    }
}
